from ui import UI

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import *
from PSO import *
import threading
import time
from EA import EA
class Example(QWidget):
    
    def __init__(self):
        super().__init__()
        self.number = 0
        self.prob = -1
        self.message = "working on it..."
        self.initUI()
        
        
    def initUI(self):
        lbl = QLabel(self)
        lbl.setText("input n = ")
        lbl.move(20, 10)
        n = QLineEdit(self)
        
        n.move(100, 10)

        # lbl = QLabel(self)
        # lbl.setText("probability =  = ")
        # lbl.move(20, 30)
        # p = QLineEdit(self)
        
        # # p.move(100, 30)
        n.textChanged[str].connect(self.getN)
        # p.textChanged[str].connect(self.getP)
        

        self.setGeometry(600, 600, 600, 440)
        self.setWindowTitle('Icon')
        self.setWindowIcon(QIcon('web.png'))  

        hc = QPushButton('Hill Climbing', self)
        hc.setToolTip('This is a <b>QPushButton</b> widget')
        hc.setCheckable(True)
        hc.resize(hc.sizeHint())
        hc.move(50, 80)
        hc.clicked[bool].connect(self.hillClimbing)

        ea = QPushButton('EA', self)
        ea.setToolTip('This is a <b>QPushButton</b> widget')
        ea.setCheckable(True)
        ea.resize(ea.sizeHint())
        ea.move(50, 120)
        ea.clicked[bool].connect(self.evolutionary)

        pso = QPushButton('PSO', self)
        pso.setToolTip('This is a <b>QPushButton</b> widget')
        pso.setCheckable(True)
        pso.resize(pso.sizeHint())
        pso.move(50, 160)
        pso.clicked[bool].connect(self.particleSwarm)
        
        self.table = QLabel(self)
        # self.table.setFixedWidth(500)
        # self.table.setText(self.message)
        # self.table.hide()
        self.table.move(150, 150)
        self.show()

    def sayHi(self):
        print(self.number, self.prob)

    def getN(self, text):
        self.number = int(text)
    def getP(self, text):
        self.prob = float(text)

    def hillClimbing(self):
        ui = UI(self.number, self.prob)
        pid = threading.Thread(target = ui.solve, args=(int(1), ))
        pid.start()
        pid.join()
        # pid.join()
        while pid.is_alive():
            #  print(pid.is_alive())
            time.sleep(0.1)
        # ui.solve(int(1))
        matrix = ""
        for i in range(self.number):
            for j in range(self.number):
                matrix= matrix+ str(ui.repository.board[i][j]) + " "
            matrix = matrix + "\n"
        self.table.setWordWrap(True)
            
        # self.table.text = "dfikguifgl'zkxcfhfvufkuifgsadghfguisdfyfgyshd"
        string = str(matrix)
        self.table.setText(string)
        self.table.resize(300,300)

    def evolutionary(self):
       
        ea = EA(int (self.number))
        
        ea.solve()
        self.table.setWordWrap(True)
        matrix = ""
        for i in range(self.number):
            for j in range(self.number):
                matrix= matrix+ str(ea.EAoutcome[i][j]) + " "
            matrix = matrix + "\n"
        # self.table.text = "dfikguifgl'zkxcfhfvufkuifgsadghfguisdfyfgyshd"
        string = "deteted minimum : " + str(ea.EAoutcomeFitness) + "\n" + str(matrix)
        self.table.setText(string)
        self.table.resize(300,300)
        # self.table.setText("dfikguifgl'zkxcfhfvufkuifgsadghfguisdfyfgyshd")
        # self.message = matrix

    def particleSwarm(self):
        first(100, self.number)
        pid = threading.Thread(target = first, args=(int(100), self.number, ))
        pid.start()
        self.table.setWordWrap(True)
        matrix = ""
        for i in range(self.number):
            for j in range(self.number):
                matrix= matrix+ str(outcome[0][i][j]) + " "
            matrix = matrix + "\n"
        # self.table.text = "dfikguifgl'zkxcfhfvufkuifgsadghfguisdfyfgyshd"
        string = "deteted minimum : " + str(outcomeFitness[0]) + "\n" + str(matrix)
        self.table.setText(string)
        self.table.resize(300,300)

        
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
# start = UI()