from controller import Controller
from repository import Repository
class UI:
    controller = None
    repository = None
    def __init__(self, n , p):
        self.repository = Repository(n)
        self.controller = Controller(self.repository)

        # self.solve()
    def solve(self, method):
        # method = int(input("type 1 for hill climbing or 2 for EA : "))
        if method == 1:
            self.controller.HillClimbing()
        elif method == 2:
            self.controller.EA(int(1000))
        else:
            print("invalid method")