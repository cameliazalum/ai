from board import Board
from s import S
from t import T
from validator import Validator

class Repository:
    n = 0
    board = None
    s = None
    t = None
    validator = None
    def __init__(self, n):
        # self.n = self.readFromFile()
        self.n = n
        print(self.n)
        self.board = Board(self.n).board
        self.s = S(self.n).s
        self.t = T(self.n).t
        self.validator = Validator(self.n)


    def readFromFile(self):
        f = open("input.txt", "r")
        return int (f.read())

    def reNew(self) :
        self.board = Board(self.n).board
        self.s = S(self.n).s
        self.t = T(self.n).t
        self.validator = Validator(self.n)
