import string
from random import *
passes = []
f = open('file.in', 'w')
characters = string.ascii_letters + string.punctuation  + string.digits
while True:
    password =  "".join(choice(characters) for x in range(randint(6, 16)))
    password = password + "\n"
    if password not in passes:
        passes.append(password)
        f.write(password)
        # print (password)