from state import State

class Problem:
    values = 0
    initialState = None
    finalState = None
    state = None
    def __init__(self):
        self.state = State()
        
        # self.finalState = State()

    def expand(self, state):
        pass

    def heuristic(self, state1, state2):
        pass

    def readFromFile(self):
        f = open("input.txt", "r")
        self.values = int (f.read())
        if self.values < 4:
            print ("no solution")
        else:
            self.initialState = self.state.setInitialState(self.values)
            self.initialState.n = self.values