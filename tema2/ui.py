from controller import Controller 


class UI:
    controller = None
    def __init__(self):
        self.controller = Controller()

    def mainMenu(self):
        
        if self.controller.instance.values > 3:
            method = int(input ("type 1 for dfs and 2 for greedy: "))
            if method == 1:
                self.controller.dfs(int(0), int(0))
            else:
                self.controller.gbfs(int(2))
        

ui = UI()
ui.mainMenu()