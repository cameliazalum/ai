from problema3 import Problem
from board import Board
import random

class Controller:
    instance = None
    board = None
    cnt = 0
    def __init__(self):
        self.instance = Problem()
        self.instance.readFromFile()
        if self.instance.values > 3:
            self.board = self.instance.initialState.board
            self.cnt = self.instance.initialState.cnt

    def orderStates(self):
        pass

    def isValid(self, board, row, column):
        sumI = 0
        sumJ = 0
        # print("board is")
        # print(board)
        # print("row and columnm are ")
        # print(row, column)
        if board[row][column] == 1:
            return False
        for i in range (self.instance.values):
            sumI = sumI + board[row][i]

        for i in range (self.instance.values):
            sumJ = sumJ + board[i][column]
              
        for i in range (self.instance.values):
            for j in range( self.instance.values):
                if abs(i - row) - abs(j - column) == 0 and board[i][j] == 1:
                    # print("diagonal")
                    # print(i, j)
                    return False 
        if sumI > 0 or sumJ > 0:
            # print ("sume = ")
            # print(sumI, sumJ)
            return False
        return True

    def printBoard(self):
        for i in self.board.board:
            for j in i:
                print(j, end = "")
            print()

    def isGoal(self):
        
        if self.cnt == self.instance.values:
            return True
        else:
            return False

    def dfs(self, start, end):
        if start < 0 or start > self.instance.values or end < 0 or end > self.instance.values:
            return
        elif self.isGoal():
            self.printBoard()
            return
        else:
            for i in range (self.instance.values):
                for j in range (self.instance.values):
                    if self.isValid(self.board.board, i, j) and self.board.visited[i][j] == 0:
                        
                        self.board.visited[start][end] = 1
                        self.board.board[i][j] = 1
                        self.cnt = self.cnt + 1
                        self.dfs(i, j)
                        self.cnt  = self.cnt -1
                        self.board.board[i][j] = 0
            
                    
    def clearBoard(self):
        self.board.board = [[0 for x in range(self.instance.values)] for y in range(self.instance.values)]
        self.board.visited = [[0 for x in range(self.instance.values)] for y in range(self.instance.values)]  
        self.cnt = 0

    def gbfs(self, problem):
        self.clearBoard()
        found = False
        visited = []
        x = random.randrange(self.instance.initialState.n - 1)
        y = random.randrange(self.instance.initialState.n - 1)
        toVisit = [[x, y]]
        valids = []
        
        while len(toVisit) > 0 and not found:
            node = toVisit.pop(0)
            visited.append(node)
            if self.isValid(self.board.board, node[0], node[1]):
                # print("board in gbfs is ")
                # self.printBoard()
                # print("\n")
                valids.append(node)
                self.board.board[node[0]][node[1]] = 1
                self.cnt = self.cnt + 1
            # print("cnt is ")
            # print(self.cnt)
            if self.isGoal():
                found = True
                self.printBoard()
            else :
                for i in range(self.instance.initialState.n):
                    for j in range (self.instance.initialState.n):
                        if [i,j] not in visited:
                            toVisit.append([i,j])

        if found == False:
            # print("no solution was found for " + str(x) + " and " + str(y))
            self.gbfs(2)
                        
