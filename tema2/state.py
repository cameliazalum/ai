from board import Board
class State:

    values = 0
    board = None
    n = 0
    cnt = 0
    def __init__(self):
        self.values = 0
    
    def setInitialState(self, n):
        self.board = Board(n)
        self.n = n
        self.cnt = 0
        return self

