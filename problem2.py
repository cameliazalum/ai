from random import random


figures = []
defaultFigures = []
m = []
numberOfAttempts = -1
solution = False
def readFigures():
    global defaultFigures
    defaultFigures.append([['1','1','1','1']])
    defaultFigures.append([['2','', '2'],['2', '2', '2']])
    defaultFigures.append([['3', '', ''], ['3', '3', '3']])
    defaultFigures.append([['4', '4', '4'], ['','','4']])
    defaultFigures.append([['', '5', ''], ['5', '5', '5']])

def isFull(matrix):
    # print("\n\n\n")
    for i in matrix:
        for j in i:
#            print(j, end = "")
            if j == '':
                return False
#        print("\n")
    return True

def fill(positions):
    global figures
    matrix = [['','','','',''],['','','','',''],['','','','',''],['','','','',''],['','','','',''],['','','','','']]
    for k in range(len(figures)):
        line = positions[k][0]
        column = positions[k][1]
        if isFull(matrix) == True:
            return matrix
        for i in figures[k]:
            
            for j in i:
                if j != '':
#                    print("matrix is")
#                    print(matrix)
#                    print("\nn\n\n\n")
                    try:
                        if matrix[line][column] == '':
                            matrix[line][column] = j
                        else:
                            return None
                    except:
                        return None
                column = column + 1
            line = line + 1
            column = positions[k][1]

def full(positions):
    matrix = fill(positions)
    global solution
    if matrix != None:
        solution = True
        global m
        m = matrix
        return True
    else:
        return False

  

def main():
    readFigures()
    global numberOfAttempts
    global figures
    global defaultFigures
    global solution
    positions = []
    if numberOfAttempts != -1:
        for k in range(numberOfAttempts):
            figures.clear()
            for i in range(9):
                figures.append(defaultFigures[int((random() * 10) % 5)])
            print(figures)
            positions.clear()
            for i in figures:
                posX = int(random() * 10) % 5
                posY = int(random() * 10) % 6
                positions.append([posX, posY])
            if full(positions) == True:
                print("a solution was found")
                for l in m:
                    for c in l:
                        print(c, end = "")
                    print("\n")
                return
        if solution == False:
            print("no solution found in this number of attempts")
    else:
        while solution == False:
            figures.clear()
            for i in range(8):
                figures.append(defaultFigures[int((random() * 10) % 5)])
            positions.clear()
            for i in figures:
                posX = int(random() * 10) % 5
                posY = int(random() * 10) % 6
                positions.append([posX, posY])
            if full(positions) == True:
                print("a solution was found")
                solution = True
                for l in m:
                    for c in l:
                        print(c, end = "")
                    print("\n")
                return

inp = input("would you like to introduce a maximum number of attempts? y/n ")
if inp == 'y':
    numberOfAttempts = int(input("the desired number is : "))

main()