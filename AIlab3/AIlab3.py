import clr
clr.AddReference('System.Drawing')
clr.AddReference('System.Windows.Forms')

from System.Drawing import *
from System.Windows.Forms import *
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow
from PyQt5.uic import loadUI


class MyForm(Form):
    def __init__(self):
        # Create child controls and initialize form
        pass


class UIObject(QMainWindow):

    def __init__(self):
        super(UIObject, self).__init__()
        loadUI('mainapp.ui', self)
        self.mybutton1.clicked.connect(self.printhello)

    def printhello():
        print("hello")

app = QApplication(sys.argv)
mainview = UIObject()
mainview.show()
sys.exit(app.exec_())


Application.EnableVisualStyles()
Application.SetCompatibleTextRenderingDefault(False)

form = MyForm()
Application.Run(form)
