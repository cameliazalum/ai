# -*- coding: utf-8 -*-

from model.board import Board
from random import randint, random
from operator import add
from math import sin, pow
from itertools import *
from model.validator import Validator

import numpy
import matplotlib.pyplot as plt

class particle:
    def __init__(self,l):
        self.validator = Validator(l)
        
        matrix = []
        for i in range(l):
            line = []
            for j in range(l):
                line.append((randint(1, l), randint(1, l )))
            matrix.append(line)
        
        self.pozition = matrix
        self.evaluate()
        self.velocity = [ 0 for i in range(l)]
        
        self.bestPozition = self.pozition.copy()
        self.bestFitness = self.fitness
 

    def fit(self,individual):
        fit = 3
        if self.validator.lineValidator(individual):
            fit = fit - 1
        if self.validator.columnValidator(individual):
            fit = fit - 1
        if self.validator.dublicatesValidator(individual):
            fit = fit - 1
        return fit

    def evaluate(self):
        self.fitness = self.fit(self.pozition)


def population(l, dimIndivid):
    pop = []
    for i in range (l):
        pop.append(particle(dimIndivid))
    return pop

    


def selectNeighbors(pop, nSize):
    if (nSize>len(pop)):
        nSize=len(pop)
    neighbors=[]
    for i in range(len(pop)):
        localNeighbor=[]
        for j in range(nSize):
            x=randint(0, len(pop)-1)
            while (x in localNeighbor):
                x=randint(0, len(pop)-1)
            localNeighbor.append(x)
        neighbors.append(localNeighbor.copy())
    return neighbors
            
    
def formBoard( p, n):
    board = Board(n).board
    for i in range(n):
        for j in range(n):
            board[i][j] = (p[i][j], p[i+n][j])
    return board


        
    
def iteration(pop, neighbors, c1, c2, w ):
    bestNeighbors=[]
    #determine the best neighbor for each particle
    for i in range(len(pop)):
        bestNeighbors.append(neighbors[i][0])
        for j in range(1,len(neighbors[i])):
            if (pop[bestNeighbors[i]].fitness>pop[neighbors[i][j]].fitness):
                bestNeighbors[i]=neighbors[i][j]
                
    #update the velocity for each particle
    for i in range(len(pop)):
        for j in range(len(pop[0].velocity)):
            newVelocity = w * pop[i].velocity[j]
            for p in range(len(pop[0].pozition[0])):
                for q in range(len(pop[0].pozition[0][0])):
                    newVelocity = newVelocity + c1*random()*(pop[bestNeighbors[i]].pozition[j][p][q]-pop[i].pozition[j][p][q])    
                    newVelocity = newVelocity + c2*random()*(pop[i].bestPozition[j][p][q]-pop[i].pozition[j][p][q])
            pop[i].velocity[j]=newVelocity
    
    #update the pozition for each particle
    for i in range(len(pop)):
        newPozition=[]
        for j in range(len(pop[0].velocity)):
            newPozition.append(pop[i].pozition[j])
        pop[i].pozition=newPozition
        pop[i].evaluate()
    return pop


def first(noIteratii, N):
    #PARAMETERS:
    n = N
    dimParticle = n
    w=1.0
    c1=1.
    c2=2.5
    sizeOfNeighborhood=40
    P = population(1000,n)
    # x= particle(n)
    # x.pozition = [[(1, 1), (2, 3), (3, 2)], [(2, 2), (3, 1), (1, 3)], [(3, 3), (1, 2), (2, 1)]]
    # P.append(x)
    # print (P)
    neighborhoods=selectNeighbors(P,sizeOfNeighborhood)
    # print(neighborhoods)
        
    for i in range(noIteratii):
        P = iteration(P, neighborhoods, c1,c2, w/(i+1))
    global outcome
    global outcomeFitness
    best = P[0]
    for i in P:
        if i.fitness<best.fitness:
            best = i
    
    fitnessOptim=best.fitness
    individualOptim=best.pozition
    outcomeFitness.append(str(fitnessOptim))
    print("Result: The detectet minimum point is " + str(individualOptim) + "  \n with function\'s value " +  str(fitnessOptim))
    outcome.append(individualOptim)
    
    if fitnessOptim > 0:
        print("sorry no solution this time :(")
    x = numpy.array([P[x].fitness for x in range(len(P))])
    y = numpy.array([x for x in range(len(P))])
        # e = numpy.array(x for x in range (4))
    plt.plot(y, x)
    plt.show()
    
outcome = []
outcomeFitness = []
# first(100, 3)
