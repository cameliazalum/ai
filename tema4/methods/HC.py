
from random import randrange, randint
from model.validator import Validator
from model.board import Board
from model.halfBoard import HalfBoard
import time
class HillClimbing:
    counter = 0
    validator = None
    def __init__(self, n):
        self.n = n
        self.counter = 0
        self.validator = Validator(self.n)
        self.board = Board(self.n).board
        self.s = HalfBoard(self.n).halfBoard.copy()
        self.t = HalfBoard(self.n).halfBoard.copy()
    

    
    def formBoard(self, p, n):
        for i in range(n):
            for j in range(n):
                self.board[i][j] = (p[i][j], p[i+n][j])
    

    def printBoard(self):
        for i in self.board:
            for j in i:
                print (j, end = " ")
            print()
            
    def reNew(self) :
        self.board = Board(self.n).board
        self.s = HalfBoard(self.n).halfBoard.copy()
        self.t = HalfBoard(self.n).halfBoard.copy()

    def hillClimbing(self):
        # self.board = [[(1, 1), (2, 3), (3, 2)], [(2, 2), (3, 1), (1, 3)], [(3, 3), (1, 2), (2, 1)]]
        x = self.board.copy()
        x1 = x.copy()
        while self.counter <= self.n * self.n:
            if self.counter == self.n * self.n:
                
                if self.validator.lineValidator(self.board) and self.validator.columnValidator(self.board) and self.validator.dublicatesValidator(self.board):
                    self.printBoard()
                    return
                else:
                    self.counter = 0
                    self.reNew()
                    x1 = self.board
            else:
                p1 = randrange(self.n)
                p2 = randrange(self.n)
                q1 = self.s[randrange(len(self.s) - 1)]
                q2 = self.t[randrange(len(self.t) - 1)]
                if x1[p1][p2] == (0,0):
                    x1[p1][p2] = (q1, q2)
                    # print (x1)
                    self.board = x1
                    self.counter = self.counter + 1
                    # print(self.counter)

            


