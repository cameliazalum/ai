"""
Sa se plimbe un cal pe o tabla de sah de dimensiune n x m, odata pe fiecare casuta.
"""

from random import *
from validator import Validator

import numpy
import matplotlib.pyplot as plt
matrix = []
class ant:
    def __init__(self, n, m, dimIndividual):
        #constructor pentru clasa ant
        self.validator = Validator(dimIndividual)
        self.dimIndividual = dimIndividual
        self.size = n * m
        self.path = [randint(0, self.size-1)]
        self.fit = self.fitness(self.dimIndividual)
        """
        drumul construit de furnica initializat aleator pe prima pozitie
        drumul este o permutare de self.size elemente, fiecare numar
        reprezentand o casuta a tablei de sah:
        pt n=4, m=6
        0  este casuta 0, 0
        1  este casuta 0, 1
        ...
        5  este casuta 0, 5
        6  este casuta 1, 0
        ...
        23 este casuta 3, 5 (ultima din cele 24 de casute)
        """
        self.n = n
        self.m = m
    def __len__(self):
        return len(self.path)
    def copy (self):
        return ant(self.n, self.m, self.dimIndividual)
    def nextMoves(self, a):
        # returneaza o lista de posibile mutari corecte de la pozitia a
        new = []
        for i in range(self.n * self.m):
            if (i not in self.path):
                new.append(i)
        return new.copy()

    def distMove(self, a):
        # returneaza o distanta empirica data de numarul de posibile mutari corecte
        # dupa ce se adauga pasul a in path
        dummy=ant(self.n, self.m, self.dimIndividual)
        dummy.path=self.path.copy()
        dummy.path.append(a)
        return (self.n*self.m-len(dummy.nextMoves(a)))
        
    def addMove(self, q0, trace, alpha, beta):
        # adauga o noua pozitie in solutia furnicii daca este posibil
        p = [0 for i in range(self.size)]
        # pozitiile ce nu sunt valide vor fi marcate cu zero
        nextSteps=self.nextMoves(self.path[len(self.path)-1]).copy()
        # determina urmatoarele pozitii valide in nextSteps
        # daca nu avem astfel de pozitii iesim 
        if (len(nextSteps) == 0):
            return False
        # punem pe pozitiile valide valoarea distantei empirice
        for i in nextSteps:
            p[i] = self.distMove(i)
        # calculam produsul trace^alpha si vizibilitate^beta
        p=[ (p[i]**beta)*(trace[self.path[-1]][i]**alpha) for i in range(len(p))]
       
        # adaugam cea mai buna dintre mutarile posibile
        p = [ [i, p[i]] for i in range(len(p)) ]
        p = max(p, key=lambda a: a[1])
        self.path.append(p[0])
       
                   
    def fitness(self, dimIndividual):
        # un drum e cu atat mai bun cu cat este mai lung
        # problema de minimizare, drumul maxim e n * m
        global matrix
        finalFitness = 4
        for i in self.path:
            individual = matrix[i]
            fit = 4
            if self.validator.lineValidator(individual):
                fit = fit - 1
                finalFitness = min(fit , finalFitness)
            if self.validator.columnValidator(individual):
                fit = fit - 1
                finalFitness = min(fit , finalFitness)
            if self.validator.dublicatesValidator(individual):
                fit = fit - 1
                finalFitness = min(fit , finalFitness)
        return finalFitness
        # return (self.size-len(self.path)+2)

def epoca(dimIndivisual, noAnts, n, m, trace, alpha, beta, q0, rho):
    antSet=[ant(n, m, dimIndivisual) for i in range(noAnts)]
    
    for i in range(n * m):
        # numarul maxim de iteratii intr-o epoca este lungimea solutiei
        for x in antSet:
            x.addMove(q0, trace, alpha, beta)
    # actualizam trace-ul cu feromonii lasati de toate furnicile
    dTrace=[1.0/antSet[i].fit for i in range(len(antSet))]
    for i in range(n * m):
        for j in range (n * m):
            trace[i][j] = (1 - rho) * trace[i][j]
    for i in range(len(antSet)):
        for j in range(len(antSet[i].path)-1):
            x = antSet[i].path[j]
            y = antSet[i].path[j+1]
            trace[x][y] = trace [x][y] + dTrace[i]
    # return best ant that attempts to reach the solution
    global matrix


    f=[ [antSet[i].fit, i] for i in range(len(antSet))]
    
    f=max(f)
    # print (f)
    # global matrix
    # print(antSet[f[1]])
    # for i in range (noAnts):
    #     print (antSet[i].path)
    return [antSet[f[1]].path, antSet[f[1]].fit]


def fillMatrixes(dimIndividual, n , m):
    global matrix
    for i in range(n * m):
        # matrix.append()
        m = []
        for ii in range(dimIndividual):
            line = []
            for jj in range(dimIndividual):
                line.append((randint(1, dimIndividual), randint(1, dimIndividual )))
            m.append(line)
        matrix.append(m)
    # matrix[len(matrix)-1] = [[(1, 1), (2, 3), (3, 2)], [(2, 2), (3, 1), (1, 3)], [(3, 3), (1, 2), (2, 1)]]
    if dimIndividual == 3:
        matrix[randint(0, n*m - 1)] = [[(1, 1), (2, 3), (3, 2)], [(2, 2), (3, 1), (1, 3)], [(3, 3), (1, 2), (2, 1)]]
    # elif dimIndividual == 8:
    #     matrix[randint(n*m - 1)] = [[(1, 1), (2, 3), (3, 2)], [(2, 2), (3, 1), (1, 3)], [(3, 3), (1, 2), (2, 1)]]

    
def main(dinIndividual, n = 20,m = 20,noEpoch = 3,noAnts = 1,alpha = 1.9,beta = 0.9,rho = 0.05,q0 = 0.5):
    fillMatrixes(dinIndividual, n , m)
    sol=[]
    bestSol=[0,0]
    trace=[[1 for i in range(n * m)] for j in range (n * m)]
    print("Programul ruleaza! Dureaza ceva timp pana va termina!")
    for i in range(noEpoch):
        sol=epoca(dinIndividual, noAnts, n, m, trace, alpha, beta, q0, rho).copy()
        # print (sol)
        if sol[1] > bestSol[1]:
            bestSol=sol.copy()
    
    
    print ("lungimea celei mai bune solutii depistate la aceasta rulare:", len(bestSol[0]))
    print ("Drumul detectat este:", bestSol)
    global matrix
    solToBePrinted = []
    bestFitness = 0
    for i in bestSol[0]:
       if fitness(matrix[i]) > bestFitness:
           bestFitness = fitness(matrix[i])
           solToBePrinted = matrix[i].copy()
    print ("solution is")
    print (solToBePrinted)
    print("its fitness is : " + str(bestFitness))
    x = numpy.array([fitness(matrix[x]) for x in range(n * m)])
    y = numpy.array([x for x in range(n*m)])
        # e = numpy.array(x for x in range (4))
    plt.plot(y, x)
    plt.show()
    # for i in matrix:
    #     print (fitness(i), i)

# ideal fitness = 4
# worst fitness = 1

def fitness(board):
        # un drum e cu atat mai bun cu cat este mai lung
        # problema de minimizare, drumul maxim e n * m
        # global matrix
        validator = Validator(len(board))
        individual = board.copy()
        fit = 1
        if validator.lineValidator(individual):
            fit = fit + 1
        if validator.columnValidator(individual):
            fit = fit + 1
        if validator.dublicatesValidator(individual):
            fit = fit + 1
        return fit

main(10)