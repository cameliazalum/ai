from math import pow
from random import *
from model.validator import Validator
import numpy
import matplotlib.pyplot as plt
class EA:
    def __init__(self, N):
        self.n = N
        self.validator = Validator(self.n)

    def individual(self,length):
        matrix = []
        for i in range(length):
            line = []
            for j in range(length):
                line.append((randint(1, self.n ), randint(1, self.n )))
            matrix.append(line)
        return matrix

    def population(self, count, length):
        return [ self.individual(length) for x in range(count) ]

        
    def fitness(self, individual):
        fit = 3
        if self.validator.lineValidator(individual):
            fit = fit - 1
        if self.validator.columnValidator(individual):
            fit = fit - 1
        if self.validator.dublicatesValidator(individual):
            fit = fit - 1
        return fit
        
    def mutate(self,individual, pM): 
        if pM > random():
                p1 = randint(0, len(individual)-1)
                q1 = randint(0, len(individual)-1)
                p2 = randint(0, len(individual)-1)
                q2 = randint(0, len(individual)-1)
                aux = individual[p1][q1]
                individual[p1][q1] = individual[p2][q2] 
                individual[p2][q2] = aux
        return individual

    def crossover(self, parent1, parent2):
        child=[]
        alpha = randint(0, len(parent1)-1)
        i = 0
        while i < len(parent1):
            if i <= alpha:
                child.append(parent1[i])
            else:
                child.append(parent2[i])
            i= i+1
        return child

    def iteration(self, pop, pM):
        i1=randint(0,len(pop)-1)
        i2=randint(0,len(pop)-1)
        if (i1!=i2):
            c = self.crossover(pop[i1],pop[i2])
            c = self.mutate(c, pM)
            f1=self.fitness(pop[i1])
            f2=self.fitness(pop[i2])
            fc=self.fitness(c)
            if(f1>f2) and (f1>fc):
                pop[i1]=c
            if(f2>f1) and (f2>fc):
                pop[i2]=c
        return pop



    def solve(self):
        dimPop = 40
        dimIndivid = self.n
        noIteratii = 1000
        pM = 0.2
        P = self.population(dimPop, dimIndivid)
        for i in range(noIteratii):
            P = self.iteration(P, pM)
        
        graded = [ (self.fitness(x), x) for x in P]
        graded =  sorted(graded)
        result=graded[0]
        fitnessOptim=result[0]
        individualOptim=result[1]
        print('Result: The detected minimum point after %d iterations is  = %3.2f'% \
            (noIteratii, fitnessOptim) )
        print ("for")
        print (individualOptim)
        if fitnessOptim > 0:
            print("sorry, no solution this time :(")
        x = numpy.array([self.fitness(P[x]) for x in range(len(P))])
        y = numpy.array([x for x in range(len(P))])
        self.EAoutcome = individualOptim
        self.EAoutcomeFitness = fitnessOptim
        plt.plot(y, x)
        plt.show()


EAoutcome = []
EAoutcomeFitness = 0
# ea = EA(int (3))
# ea.solve()