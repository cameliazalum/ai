
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import *
import threading
import time
from service.controller import Controller

class UI(QWidget):
    
    def __init__(self):
        super().__init__()
        self.number = 0
        self.prob = -1
        self.message = "working on it..."
        self.initUI()
        
        
    def initUI(self):
        lbl = QLabel(self)
        lbl.setText("input n = ")
        lbl.move(20, 10)
        n = QLineEdit(self)
        
        n.move(100, 10)

        # lbl = QLabel(self)
        # lbl.setText("probability =  = ")
        # lbl.move(20, 30)
        # p = QLineEdit(self)
        
        # # p.move(100, 30)
        n.textChanged[str].connect(self.getN)
        # p.textChanged[str].connect(self.getP)
        

        self.setGeometry(600, 600, 600, 440)
        self.setWindowTitle('Icon')
        self.setWindowIcon(QIcon('web.png'))  

        hc = QPushButton('Hill Climbing', self)
        hc.setToolTip('This is a <b>QPushButton</b> widget')
        hc.setCheckable(True)
        hc.resize(hc.sizeHint())
        hc.move(50, 80)
        hc.clicked[bool].connect(self.hillClimbing)

        ea = QPushButton('EA', self)
        ea.setToolTip('This is a <b>QPushButton</b> widget')
        ea.setCheckable(True)
        ea.resize(ea.sizeHint())
        ea.move(50, 120)
        ea.clicked[bool].connect(self.evolutionary)

        pso = QPushButton('PSO', self)
        pso.setToolTip('This is a <b>QPushButton</b> widget')
        pso.setCheckable(True)
        pso.resize(pso.sizeHint())
        pso.move(50, 160)
        pso.clicked[bool].connect(self.particleSwarm)
        
        self.table = QLabel(self)
        # self.table.setFixedWidth(500)
        # self.table.setText(self.message)
        # self.table.hide()
        self.table.move(150, 150)
        self.show()

    def sayHi(self):
        print(self.number, self.prob)

    def getN(self, text):
        self.number = int(text)
       

    def getP(self, text):
        self.prob = float(text)

    def hillClimbing(self):
        self.controller= Controller(self.number)
        self.controller.hillClimbingSolve()
        # pid.join()
       
        matrix = ""
        for i in range(self.number):
            for j in range(self.number):
                matrix= matrix+ str(self.controller.solution[0][i][j]) + " "
            matrix = matrix + "\n"
        self.table.setWordWrap(True)
            
        # self.table.text = "dfikguifgl'zkxcfhfvufkuifgsadghfguisdfyfgyshd"
        string = str(matrix)
        self.table.setText(string)
        self.table.resize(300,300)

    def evolutionary(self):
        self.controller= Controller(self.number)
        self.controller.eaSolve()
        
        self.table.setWordWrap(True)
        matrix = ""
        for i in range(self.number):
            for j in range(self.number):
                matrix= matrix+ str(self.controller.solution[1][i][j]) + " "
            matrix = matrix + "\n"
        string = "deteted minimum : " + str(self.controller.solution[0]) + "\n" + str(matrix)
        self.table.setText(string)
        self.table.resize(300,300)

    def particleSwarm(self):
        self.controller= Controller(self.number)
        self.controller.psoSolve()
        self.table.setWordWrap(True)
        matrix = ""
        for i in range(self.number):
            for j in range(self.number):
                matrix= matrix+ str(self.controller.solution[1][i][j]) + " "
            matrix = matrix + "\n"
        string = "deteted minimum : " + str(self.controller.solution[0]) + "\n" + str(matrix)
        self.table.setText(string)
        self.table.resize(300,300)

    def aco(self):
        pass
        
