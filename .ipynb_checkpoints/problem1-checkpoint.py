# import uniform distribution
from scipy.stats import uniform
from scipy.stats import binom
import seaborn as sns

from random import random
def main():
    
    
    print("1-binomial distribtion\n2-uniform distribution")
    option = input('type your option: ')
    
 
    if option == "2":
        intervalA = int(input("choose the interval lower bound: "))
        intervalB = int(input("choose the interval upper bound: "))
        n = random()
        dataUniform = uniform.rvs(size=int(n*1000), loc = intervalA, scale = intervalB - intervalA)
        ax = sns.distplot(dataUniform,
                  bins=100,
                  kde=True,
                  color='skyblue',
                  hist_kws={"linewidth": 10,'alpha':1})
        ax.set(xlabel='Uniform Distribution ', ylabel='Frequency')
    elif option == "1":
        intervalB = int(input("choose the interval upper bound: "))
        n = random()
        probability = random()
        dataBinom = binom.rvs(n = intervalB, p = probability, size = int(n*1000))
        ax = sns.distplot(dataBinom,
                  kde=False,
                  color='skyblue',
                  hist_kws={"linewidth": 10,'alpha':1})
        ax.set(xlabel='Binomial Distribution', ylabel='Frequency')
            
    
    
main()